# P1-doc

The following sections will describe each technical aspect used throughout the practice such as developed tests by each microservice, coverage reports, pipeline CI, deploy and methodology used...



## Testing

At the beginning, project had not started using testing methodologies and there is legacy code to start testing.

This is a small problem because there were code to be refactored before testing. There are some methods that there are really difficult to be tested.

Some different testing technics have been used such as **fixtures**, **parametrization**, **mocks**,...

#### Fixtures

There are some fixtures implemented on the project to **reset_database**, **seed_database**, **create_token** to be authenticated with *auth* microservice,...

![](img/fixtures_test.png)

Chosen fixture type in some of them have been **session** because that action is only needed once at the beginning.

In some cases that database has to be cleared, it depends with test function that executes. So, chosen fixture type is **function**.

#### Parametrized Tests

This technic could be used to accomplish with some extreme values in **corner tests**. For example, if some function has a lot of *if* *else* inside, a test can be parametrized with different values in order to obtain a 100% of coverage inside that function.

![](img/parametrization_tests.png)

In this project a simple example has been done. Some different users are created to check how username is changed in the responsed json item

#### Minimum coverage permitted

Minimum coverage on the rubric is to obtain at least a 20% of coverage.

At the end, obtained coverage has been a little bit higher with a 50% of coverage. Each coverage percentage is at follows:

* Auth: 53%
* Machine: 73%
* Payment: 57%
* Delivery: 54%
* Logger: 72%
* Store: 53%
* Order: 62%

Each microservice coverage is over 50%. In case that the obtained percentage is less, CI/CD pipeline will fail because tests inside **CI pipeline** are run with `--cov-fail-under=50` flag activated.

All microservices coverage reports can be seen in `microservices/reports` folder. 

![](img/coverage_main.png)

![](img/coverage_1.png)

Code to generate all reports is

```sh
~$ docker exec microservicios_auth_1 python -m pytest 'test' -p no:warnings -s --cov=application --cov-report html:/reports/auth
~$ docker exec microservicios_machine_1 python -m pytest 'test' -p no:warnings -s --cov=application --cov-report html:/reports/machine
~$ docker exec microservicios_payment_1 python -m pytest 'test' -p no:warnings -s --cov=application --cov-report html:/reports/payment
~$ docker exec microservicios_delivery_1 python -m pytest 'test' -p no:warnings -s --cov=application --cov-report html:/reports/delivery
~$ docker exec microservicios_log_1 python -m pytest 'test' -p no:warnings -s --cov=application --cov-report html:/reports/log
~$ docker exec microservicios_store_1 python -m pytest 'test' -p no:warnings --cov=application --cov-report html:/reports/store
~$ docker exec microservicios_order_1 python -m pytest 'test/normal' -p no:warnings -s --cov=application --cov-report html:/reports/order
```

*See reports*
* [Auth](microservicios/reports/auth/index.html)
* [Machine](microservicios/reports/machine/index.html)
* [Payment](microservicios/reports/payment/index.html)
* [Delivery](microservicios/reports/delivery/index.html)
* [Log](microservicios/reports/log/index.html)
* [Store](microservicios/reports/store/index.html)
* [Order](microservicios/reports/order/index.html)

#### Mocking

This technique is used to mock application methods in order to get an appropriate response.

For example, in order project, a unit test it is desirable to be isolated of the rest microservices. So, to be authenticated, a method in charge of checking JWT token is mocked to have needed permissions to accomplish the task.

![](img/test_mocking.png)

## Smoke test suite

This kind of tests are used to check the integration of microservices.

Some differences between this kind of tests and unit tests are next ones:

* In *smoke testing* requests are made in a usual flow of the application. This means, each request has been done to API Gateway to check how HAProxy and Consul are working fine. In *unit testing* requests are done inside a testing session
* In *smoke testing* coverage is not used. It is supposed that a good coverage has been obtained in previous CI/CD pipeline stages.

![](img/smoketests.png)

In order to run smoke tests, run next code

```sh
~$ docker-compose up -d
~$ docker exec microservicios_order_1 python -m pytest 'test/smoke' -p no:warnings
```

## Cancel order saga with TDD

When a client make an order, Create Order Saga starts working sending and receiving messages to/from all microservices in an asynchronous way. There is a small time where the user should cancel manually the order if it has not been sent by *delivery* microservice yet.

That functionality is one implemented following TDD methodology.

**Steps for TDD implementation** are next ones:

* **Phase 1**: implement test_cancel_order.py

* commit + push = test **fails** in commit *[#e0d0ade9](https://gitlab.com/iker.ocio/ikerocio-idc-p1/-/commit/e0d0ade94402fe786493080c72c89664b53645da)*

  ![](img/test_cancel_fails.png)

* **Phase 2**: implement new functionality

* commit + push = test now **works** in commit *[#fc4fa916](https://gitlab.com/iker.ocio/ikerocio-idc-p1/-/commit/fc4fa9162a2d47f27812b49998a8f1f0ceb1a39c)*

  ![](img/test_cancel_passes.png)



It is not necessary (neither recommendable) to push a pipeline that you know will fail. In this case it has been done to check that TDD has been implemented.


## Pipeline CI

This functionality has been implemented from the beginning. *.gitlab-ci.yml* file was configured from the first commit in order to modify (and check) the default configuration from Gitlab control panel.

![](img/pipeline_ci.png)

**Pipeline has 4 stages:**

* **Build**: this stage is executed in all branches

  * This stage builds docker images of all microservices (10 in total). An **inmutable** image is **pushed** to gitlab container registry

* **Test**: this stage is executed in all branches

  * This stage tests all microservices (except Consul, HAProxy and RabbitMQ) with different techniques. All tests are isolated from the infrastructure.

* **Delivery**: this stage only is executed in **MASTER** branch

  * If all tests have been success, Docker images are retagged with release version as follows:  **release_0.1.0 **. [*SEMVER*](https://semver.org/) standard it follows to tag the images with MAJOR.MINOR.PATCH.

    ![](img/registry_semver.png)

  * Release image also is tagged as **latest**. So, in the future, we will have many release_X.X.X versions but just one latest version.

* **Staging**: this stage only is executed in **MASTER** branch

  * This stage check the microservices with some smoking tests. The most bigger difference between this tests and tests in *Test* stage is that here the infrastructure is tested too. All requests are made to HAProxy and tests are running in a **real environment**.

### Expiration policy

Container warehouse keep all Docker images by default without expiration policy. This means that we will have many images occupying space and most of them will be test images.

So, an expiration policy has been configured to remove old images tagged by **test_** retaining one image.

![](img/expiration_policy.png)

### Custom Badges

2 new badges has been added.

* A coverage option has been configured on Gitlab control panel in order to get the coverage measurement using a *REGEX* in the last pipeline job
* A custom badge has been defined in order to see in which app version are in the moment of watching repository. This custom badge sometimes does not work in Gitlab web page, but if you are watching this markdown document locally, there should not be any problem.

![](img/tags.png)

### Dockerfiles and Docker-compose files

Some files have been defined to accomplish all tasks.

In order to create an immutable docker Images, each microservice is builded in the pipeline with custom Dockerfiles (by default, builded image is the same with different volumes in order to increase development speed) . Those files are in `microservicios/Dockerfiles.prod` folder

* Dockerfile.auth
* Dockerfile.delivery
* Dockerfile.logger
* Dockerfile.machine
* Dockerfile.order
* Dockerfile.payment
* Dockerfile.store

Those Dockerfiles are used by an specific docker-compose file inside the Pipeline. Requirements.txt layer is cached between Dockerfiles.

There are also defined some docker-compose files in `microservicios` folder path.

* **docker-compose.yml**: This file build each microservice in development time. As it has been said, microservices use the same image and just the *volume* is different from each other.
* **docker-compose.build.yml**: This file build each microservice with its own Dockerfile.X (as exaplined before) and *tag* the image with its own specific tag defined in *.gitlab-ci.yml*
* **docker-compose.test.yml**: This file pulls each microservice test image (tagged before) in order to run tests
* **docker-compose.staging.yml**: This file pulls each microservice latest image (tagged before, got from environment variable) in order to run smoke tests
* **docker-compose.deployment.yml**: This file pulls each microservice latest image in order to be deployed from Cloudformation stack file.


### Things to improve

In general words, Pipeline time needs to be improved. 

Total time of 4 stages is up to **17min 18secs** splitted as follows:

* **Build**: 4min 4sec
* **Test**: 4min 27sec
* **Delivery**: 2min 25sec
* **Staging**: 6min 20sec

There are some ideas to reduce this time in future iterations. These ideas are described as follows:
* Joining **Build** and **Test** in one stage could reduce from 2 to 3 minutes.
* Joining **Delivery** and **Staging** in one stage could reduce up to 2 minutes
* If smokes tests are launched in a different machine with more performance, 2-3 minutes could be reduced. This is because there are some *sleep()* times added because current machine performance is poor and all microservices need more time to stay read
* Using a personal runner could increment performance. Improvement estimation does not known.

Taking those ideas, CI/CD pipeline time could stay in 10 minutes more or less.



## Manual deploy

![](img/deploy.png)

All files related to manual deploy are in `devops` folder. Next steps assume that you are inside this folder path.

Here you can find 3 main files.

* **cloudformation.yaml**: cloudformation stack file which is going to be uploaded on S3 bucket
* **cloudformation.staging.yaml**: cloudformation stack file to use in staging phase of CI pipeline. IP address has to be set in Gitlab environment.
* **createStackAWS.sh**: a script to start stack creation with a name and S3 filename given
* **deleteStackAWS.sh**: a script to delete an AWS stack

**Steps to deploy**
```sh
~$ aws s3 cp ./cloudformation.yaml s3://mac2020-cicd
~$ ./createStackAWS.sh deployment https://mac2020-cicd.s3.amazonaws.com/cloudformation.yaml
```

**Steps to Stage**
Launch staging server
```sh
~$ aws s3 cp ./cloudformation.staging.yaml s3://mac2020-cicd
~$ ./createStackAWS.sh staging https://mac2020-cicd.s3.amazonaws.com/cloudformation.staging.yaml
```

Then, add IP address in **AWS_IP** variable in Gitlab
![](img/variables.png)

Now you can start Pipeline into master

**Steps to remove stack**
```sh
~$ ./deleteStackAWS.sh deployment|staging
```

## Gitlab-runner

Here there are some commands relative of Gitlab runner

**Install a new runner with another image**
```sh
~$ sudo gitlab-runner register \
--non-interactive \
--executor "docker" \
--docker-image "docker:stable" \
--url "https://gitlab.com/" \
--registration-token "QHc3F9jaUpmE5bN2vy3s" \
--description "docker-runner-pc-casa" \
--tag-list "docker,aws,casa" \
--run-untagged \
--locked="false"
```

Then, set privilege mode to true modifying `sudo nano /etc/gitlab-runner/config.toml` with next code
```
[[runners]]
  name = "docker-runner"
  url = "https://gitlab.com/"
  token = "QHc3F9jaUpmE5bN2vy3s"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
```

And then restart service with `sudo service gitlab-runner restart`

**Uninstall runners**

```sh
~$ sudo gitlab-runner unregister --url http://gitlab.com/ --token u2U3s9p8mn6dWX-6Zg4J
```

**View registered runners**

```sh
~$ sudo gitlab-runner list
```

**Verify** if there are runner removed on gitlab but **not** in host

```sh
~$ sudo gitlab-runner verify
```

If there is any message like 
![](img/runner_verify.png)
it means that remote runner is removed but not from local host.
**To delete,**

```sh
sudo gitlab-runner verify --delete
```

## Agile

#### Branch strategy

**Github flow** is the chosen methodology to accomplish this project. 

**Master**, **dev** and some **features branches** have been used during the development of the project. Features branches have been created from dev branch when issues related with Order Cancel Saga and tests Coverage have had to be done.

It has tried to use master branch just in more stable moment of the code but sometimes in order to check the pipeline, some merge request to master have been done.

#### Board

![](img/board.png)

All project have been done in just 1 milestone. It has been decided to simplify the tasks related to the number of milestones in order to work with other concepts such as tests and pipelines.

Issues have been closed with commits keywords like `Closes #4` 

#### Retrospective meeting

I have spent more time that I would like, preparing the old project with legacy code for testing purposes. I would like to invest more time with the other things like testing and so on but the graded lab has been a good one.

A lot of tasks related with CI pipeline have been done in order to discover new functionality apart from concepts of the class. Things like custom badges, docker-in-a-docker, artifacts, testing, mocking,... have been very interesting.

I'm looking forward to learning more about CI/CD.