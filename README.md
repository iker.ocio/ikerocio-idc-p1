# P1-Validated container delivery

Master branch:

[![pipeline status](https://gitlab.com/iker.ocio/ikerocio-idc-p1/badges/master/pipeline.svg)](https://gitlab.com/iker.ocio/ikerocio-idc-p1/-/commits/master) [![coverage report](https://gitlab.com/iker.ocio/ikerocio-idc-p1/badges/master/coverage.svg)](https://gitlab.com/iker.ocio/ikerocio-idc-p1/-/commits/master) [![app_version](https://img.shields.io/badge/dynamic/json.svg?label=app_version&url=http://gitlab.com/iker.ocio/ikerocio-idc-p1/raw/master/badges.json&query=app_version)]()

Dev branch:

[![pipeline status](https://gitlab.com/iker.ocio/ikerocio-idc-p1/badges/dev/pipeline.svg)](https://gitlab.com/iker.ocio/ikerocio-idc-p1/-/commits/dev) [![coverage status](https://gitlab.com/iker.ocio/ikerocio-idc-p1/badges/dev/coverage.svg)](https://gitlab.com/iker.ocio/ikerocio-idc-p1/-/commits/dev)

## Introduction

This task goal is to implement **CI** (not CD) on a microservices project with **TDD** in a multi-environment context.

**SCRUM** methodology is going to be used to agalize tasks implementation. Joseba and Urtzi are going to be **clients** profile to supervise the project. Inside Gitlab, they will have **Reporter** role.



*Estimated completion date: March 26, 2020* 

## Getting Started

This project consist on a **microservice's** architecture application.

There are multiple services such us **HAProxy** (load balancer/api gateway), **Consul** (service discovery), **CA Authority** (RSA certs generator) and business logic some microservices.

![](img/service-discovery-pattern.png)

All services are in a Docker-compose file for a simple deployment of the infrastructure.

* **Prerequisites**
  * Linux host
  * Docker and Docker-compose
* **Installation**
  * For a **basic** usage, just type ```docker-compose up -d``` in the main path of the project.
  
  * For a **complete** deployment, it is needed a cloud provider such as AWS, GCloud, Heroku... and deploy each microservice on one (at least) or more different hosts.
  
    Inside `devops` folder, there is a script to deploy a Cloudformation stack on AWS (host machine needs AWS role credentials)
* **Usage**
  
  * It is needed an API client to make requests. For example: ```Postman```
```sh
# authenticate
https://10.100.199.200:14000/auth/authenticate -> return Token
username: admin
password: admin

# create payment
https://10.100.199.200:14000/payment -> Bearer Token needed
user_id: admin
balance: 20000

# create order
https://10.100.199.200:14000/order -> Bearer Token needed
client_id: admin
number_of_pieces: 4
type: type_a
address: 01
```

* **Testing**
  * For testing there are some functions prepared to run
```sh
~$ docker exec microservicios_auth_1 python -m pytest 'test' -p no:warnings --cov=application
~$ docker exec microservicios_auth_1 python -m black ./
~$ docker exec microservicios_auth_1 python -m flake8
~$ docker exec microservicios_auth_1 /bin/sh -c "isort */*.py"
```
## Documentation

**Development documentation:** [doc.md](doc.md) 

This project has been divided on 1 milestone with multiple tasks/issues. The following tasks are going to be defined on a **Gitlab Board** in different **issues** object.

* **Milestone 1**
  1. *(25%)* A basic **unit test suite** with a **coverage** of 20% (at least).
  2. *(15%)* A **smoke test suite** for the project
  3. *(15%)* A **new functionality** implemented with **TDD** methodology
  4. *(30%)* Create a new **CI pipeline** with build and test process automated. As a result of the pipeline, useful containers are going to be available.
  5. *(5%)* A manual deployment in **Stage** phase using SSH connection.
  6. *(10%)* **Agile methodology** used during this project work phase.

* **Comments**
  1. Used **#noqa** to ignore some Flake8 false positives. For example, auth/__init__.py#29
  2. Machine thread stop method implemented for testing session. For example, in machine project, conftest.py is used.
  3. RabbitMQ EventHandler not starts in Testing config mode. Another workaround it could be start thread with self.daemon = True. This is to avoid to the testing session stay blocked

## Authors

**Maintainer:** Iker Ocio

**Reporters**: Joseba Andoni Agirre, Urtzi Markiegi

## License

The MIT License



Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.