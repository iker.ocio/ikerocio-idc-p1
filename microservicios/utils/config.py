from os import environ

import netifaces as ni


class BaseConfig:
    TESTING = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False  # new


class DevelopmentConfig(BaseConfig):
    SQLALCHEMY_DATABASE_URI = environ.get("DATABASE_URI")
    SQLALCHEMY_TRACK_MODIFICATIONS = environ.get("SQLALCHEMY_TRACK_MODIFICATIONS")
    CONSUL_IP = environ.get("CONSUL_HOST")
    SERVICE_ID = ""
    HOST_IP = environ.get("HOST_IP", "")
    PORT = int(environ.get("GUNICORN_PORT"))
    SERVICE_NAME = environ.get("SERVICE_NAME")
    SERVICE_NAME_UPPERCASE = environ.get("SERVICE_NAME").upper()
    LOG_LEVEL = int(environ.get("LOG_LEVEL"))
    IP = None

    __instance = None

    def get_ip(self):
        if self.HOST_IP:
            self.IP = self.HOST_IP
            print(self.IP)
        else:
            print("HOST IP NOT DEFINED")
            exit(1)

    @staticmethod
    def get_instance():
        if DevelopmentConfig.__instance is None:
            DevelopmentConfig()
        return DevelopmentConfig.__instance

    def __init__(self):
        """ Virtually private constructor. """
        if DevelopmentConfig.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            self.get_ip()
            DevelopmentConfig.__instance = self

        self.SERVICE_ID = self.SERVICE_NAME + "-" + str(self.IP)

    @staticmethod
    def get_ip_iface(iface):
        return ni.ifaddresses(iface)[ni.AF_INET][0]["addr"]


class TestingConfig(BaseConfig):
    TESTING = True
    SQLALCHEMY_DATABASE_URI = environ.get("DATABASE_TEST_URI")


app_config = {
    "development": DevelopmentConfig,
    "testing": TestingConfig
    # 'staging' : StagingConfig,
    # 'production' : ProductionConfig
}
