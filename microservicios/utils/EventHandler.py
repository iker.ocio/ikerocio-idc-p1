#!/usr/bin/env python
import ssl
import time
from os import environ
from threading import Thread

import pika
from pika import exceptions

from .logger import Logger

logger = Logger("Event Handler")


class EventHandler(Thread):
    def __init__(self, exchange, routing_key, type, callbackFunc):
        Thread.__init__(self)
        self.daemon = True
        while True:
            try:
                logger.print_no_log("Trying rabbit connection", Logger.CONNECTION)
                context = ssl.create_default_context(
                    cafile=environ.get("RABBITMQ_CA_CERT_LOCATION")
                )
                context.load_cert_chain(
                    environ.get("RBT_CERT_LOCATION"), environ.get("RBT_KEY_LOCATION")
                )
                ssl_options = pika.SSLOptions(context, "rabbitmq")

                self.connection = pika.BlockingConnection(
                    pika.ConnectionParameters(
                        host=environ.get("RABBITMQ_IP"),
                        port=environ.get("RABBITMQ_PORT_HTTPS"),
                        ssl_options=ssl_options,
                    )
                )
            except exceptions.AMQPConnectionError:
                logger.print_no_log("Error connecting to RMQ", Logger.CONNECTION)
                time.sleep(5)
                continue

            logger.print_no_log("Connection established", Logger.CONNECTION)
            break

        self.channel = self.connection.channel()
        self.type = type
        self.callbackFunc = callbackFunc
        self.channel.exchange_declare(
            exchange=exchange, exchange_type=self.type, durable=True
        )
        result = self.channel.queue_declare(queue="", durable=True)
        self.channel.queue_bind(
            exchange=exchange, queue=result.method.queue, routing_key=routing_key
        )
        self.channel.basic_consume(
            queue=result.method.queue, auto_ack=False, on_message_callback=self.callback
        )
        self.start()

    def run(self):
        logger.print_no_log(
            "[*] Waiting for messages. To exit press CTRL+C", Logger.CONNECTION
        )
        self.channel.start_consuming()

    # flake8: noqa
    def callback(self, ch, method, properties, body):
        logger.print_no_log("[x] Received %r" % body, Logger.CONNECTION)
        ch.basic_ack(delivery_tag=method.delivery_tag)

    def callback(self, ch, method, properties, body):
        logger.print_no_log("[x] Received %r" % body, Logger.CONNECTION)
        self.callbackFunc(body)
        ch.basic_ack(delivery_tag=method.delivery_tag)
