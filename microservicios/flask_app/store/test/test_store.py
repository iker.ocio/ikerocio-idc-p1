import json

"""
docker exec microservicios_store_1 python -m pytest 'test' -p no:warnings --cov=application -s
"""

keys = ["order_list", "piece_list", "status"]


def test_check_store_status_with_permission(create_token, test_app):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/store/status", headers=headers,)
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    print(response_json)
    for key in keys:
        assert key in response_json


def test_health_check_with_permission(create_token, test_app):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/health", headers=headers,)
    assert resp.status_code == 200
    assert resp.data.decode() == "OK"


def test_pieces_with_permission(create_token, test_app):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/pieces", headers=headers,)
    assert resp.status_code == 200
