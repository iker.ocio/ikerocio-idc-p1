import json
import time
import os
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from utils.EventPublisher import EventPublisher
from .models import Piece
from .api_client import get_auth_public_key
from utils.logger import Logger
from utils.BLConsul import BLConsul

logger = Logger("__init__")

db = SQLAlchemy()

from .store import StoreHouse  # noqa
from .businessLogic import BusinessLogic  # noqa


def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])


def callback(msgRaw):
    jsonMsg = json.loads(msgRaw)
    new_order = {
        "order_id": int(jsonMsg["order_id"]),
        "number_of_pieces": int(jsonMsg["number_of_pieces"]),
        "type": str(jsonMsg["type"]),
    }
    storeHouse = StoreHouse.get_instance()
    storeHouse.add_order_to_queue(new_order)
    logger.print(
        msg="event: order with id "
        + str(jsonMsg["order_id"])
        + " added to store queue",
        level=Logger.INFO,
    )


def callback_piece(msgRaw):
    jsonMsg = json.loads(msgRaw)
    piece_id = int(jsonMsg["piece_id"])
    BusinessLogic.get_instance().update_piece_status(
        piece_id, Piece.STATUS_MANUFACTURED
    )
    piece = BusinessLogic.get_instance().get_piece(piece_id)
    storeHouse = StoreHouse.get_instance()
    storeHouse.add_piece_to_queue(piece)
    logger.print(
        msg="event: piece with id "
        + str(jsonMsg["piece_id"])
        + " added to store queue",
        level=Logger.INFO,
    )


def callback_cancel_order(msgRaw):
    jsonMsg = json.loads(msgRaw)
    eventPublisherStoreCancel = EventPublisher("store_remove_done", "fanout")
    order_id = int(jsonMsg["order_id"])
    logger.print(
        msg="event: order with id " + str(jsonMsg["order_id"]) + " requested cancel",
        level=Logger.INFO,
    )
    storeHouse = StoreHouse.get_instance()
    if storeHouse.working_order == order_id:
        eventPublisherStoreCancel.send_data(json.dumps({"event": "error"}), "")
    else:
        storeHouse.remove_order_from_queue(order_id)
        logger.print(
            msg="event: order with id "
            + str(jsonMsg["order_id"])
            + " deleted from queue",
            level=Logger.INFO,
        )
        eventPublisherStoreCancel.send_data(json.dumps({"event": "done"}), "")


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app_settings = os.getenv("APP_STAGE")
    app.config.from_object(app_settings)
    db.init_app(app)

    with app.app_context():
        from . import routes  # noqa
        from . import models

        # store = StoreHouse.get_instance() # noqa
        while True:
            message = "Trying getting Pubkey"
            logger.print(msg=message, level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                message = "Error getting Auth Pubkey"
                logger.print(msg=message, level=Logger.DEBUG)
                time.sleep(20)
                continue

            message = "Pubkey succesfully getted"
            logger.print(msg=message, level=Logger.DEBUG)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"], app)
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        models.Base.metadata.create_all(db.engine)

        if app_settings != "utils.config.TestingConfig":
            from utils.EventHandler import EventHandler

            EventHandler(
                exchange="auth_pubkey",
                routing_key="",
                type="fanout",
                callbackFunc=callbackUpdatePubkey,
            )
            EventHandler(
                exchange="store_saga",
                routing_key="",
                type="fanout",
                callbackFunc=callback,
            )
            EventHandler(
                exchange="store",
                routing_key="",
                type="fanout",
                callbackFunc=callback_piece,
            )
            EventHandler(
                exchange="store_remove",
                routing_key="",
                type="fanout",
                callbackFunc=callback_cancel_order,
            )

        BusinessLogic.get_instance().set_up_status(True)
        return app
