import requests
import base64
import os
import unittest
from time import sleep

HAPROXY_IP = "https://192.168.17.3:14000"
HAPROXY_IP = os.environ.get('AWS_IP', '192.168.17.3')
HAPROXY_IP = "https://" + HAPROXY_IP + ":14000"

SLEEP_TIME_1 = 40
print("SETTED TIME: " + str(SLEEP_TIME_1))

class OrderTestCase(unittest.TestCase):
    """ This class represent the Orders test case"""

    """
    This method authenticate with a username/password given
    """

    def authenticate(self, username, password):
        encoded_username = (
            username.encode("utf-8") + ":".encode("utf-8") + password.encode("utf-8")
        )
        base64_credentials = base64.b64encode(encoded_username).decode("utf-8")
        headers = {"Authorization": "Basic " + base64_credentials}
        url = HAPROXY_IP + "/auth/authenticate"
        resp = requests.post(url, headers=headers, verify=False)
        token = resp.json()["access_token"]
        return {"Authorization": "Bearer " + token}

    def payment(self, username):
        url = HAPROXY_IP + "/payment"
        resp = requests.post(
            url,
            headers=self.headers,
            json={"user_id": username, "balance": 20000},
            verify=False,
        )
        assert resp.status_code == 200

    def setUp(self):
        self.headers = self.authenticate("admin", "admin")
        self.payment("admin")
        """ Define test variables and initialize app """
        print("Set up")

    def tearDown(self):
        print("Finished\n")

    def test_cancel_order(self,):
        print("Create order test...")
        url = HAPROXY_IP + "/order"
        client_id = "admin"
        resp = requests.post(
            url,
            headers=self.headers,
            json={
                "client_id": client_id,
                "number_of_pieces": 4,
                "type": "type_a",
                "address": "01",
            },
            verify=False,
        )

        assert resp.status_code == 200
        print("Order created")
        response = resp.json()
        order_id = response["id"]

        self.cancel_order(order_id, client_id)
        sleep(SLEEP_TIME_1)
        self.check_order_is_totally_cancelled(order_id)

    def cancel_order(self, order_id, client_id):
        url = HAPROXY_IP + "/order/" + str(order_id)
        resp = requests.delete(
            url, headers=self.headers, json={"client_id": client_id}, verify=False
        )
        assert resp.status_code == 200
        assert resp.json()["cancelled"] == "requested"

    def check_order_is_totally_cancelled(self, order_id):
        status = self.get_order_status(order_id)
        assert status == "Cancelled - Manual Cancel"

    def get_order_status(self, order_id):
        resp = requests.get(
            HAPROXY_IP + "/order/orders", headers=self.headers, verify=False
        )
        for order in resp.json():
            if order["id"] == order_id:
                return order["status"]

        return None
