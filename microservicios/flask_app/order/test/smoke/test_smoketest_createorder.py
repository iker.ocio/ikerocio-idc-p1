import requests
import base64
import os
import unittest
from time import sleep

HAPROXY_IP = "https://192.168.17.3:14000"
HAPROXY_IP = os.environ.get('AWS_IP', '192.168.17.3')
HAPROXY_IP = "https://" + HAPROXY_IP + ":14000"
#HAPROXY_IP = "https://52.91.113.76:14000"

SLEEP_TIME_1 = 40
print("SETTED TIME: " + str(SLEEP_TIME_1))

order_status = [
    {"name": "Init", "passed": False},
    {"name": "Payed", "passed": False},
    {"name": "Approved", "passed": False},
    {"name": "Created", "passed": False},
    {"name": "Delivered", "passed": False},
    {"name": "Finished", "passed": False},
]

order_status_no_money = [
    {"name": "Init", "passed": False},
    {"name": "No Money", "passed": False},
    {"name": "Rejected", "passed": False},
]


class OrderTestCase(unittest.TestCase):
    """ This class represent the Orders test case"""

    orders_to_delete = []

    """
    This method authenticate with a username/password given
    """

    def authenticate(self, username, password):
        encoded_username = (
            username.encode("utf-8") + ":".encode("utf-8") + password.encode("utf-8")
        )
        base64_credentials = base64.b64encode(encoded_username).decode("utf-8")
        headers = {"Authorization": "Basic " + base64_credentials}
        url = HAPROXY_IP + "/auth/authenticate"
        resp = requests.post(url, headers=headers, verify=False)
        token = resp.json()["access_token"]
        return {"Authorization": "Bearer " + token}

    def payment(self, username):
        url = HAPROXY_IP + "/payment"
        resp = requests.post(
            url,
            headers=self.headers,
            json={"user_id": username, "balance": 20000},
            verify=False,
        )
        assert resp.status_code == 200

    def setUp(self):
        self.headers = self.authenticate("admin", "admin")
        self.payment("admin")
        """ Define test variables and initialize app """
        print("Set up")

    def tearDown(self):
        print("Finished\n")

    def test_create_order(self,):
        print("Create order test...")
        url = HAPROXY_IP + "/order"
        resp = requests.post(
            url,
            headers=self.headers,
            json={
                "client_id": "admin",
                "number_of_pieces": 4,
                "type": "type_a",
                "address": "01",
            },
            verify=False,
        )

        assert resp.status_code == 200
        response = resp.json()
        order_id = response["id"]
        self.orders_to_delete.append(order_id)
        assert self.is_order_exists(order_id)

        assert self.is_order_initialized(order_id)
        print("Waiting " + str(SLEEP_TIME_1) + " seconds until the order has finished...")
        sleep(SLEEP_TIME_1)

        self.is_order_finished(order_id)

    def is_order_finished(self, order_id):
        assert self.get_order_status(order_id) == "Finished"

    def is_order_exists(self, order_id):
        return self.get_order_status(order_id) is not None

    def get_order_status(self, order_id):
        resp = requests.get(
            HAPROXY_IP + "/order/orders", headers=self.headers, verify=False
        )
        for order in resp.json():
            if order["id"] == order_id:
                return order["status"]

        return None

    def is_order_initialized(self, order_id):
        resp = requests.get(
            HAPROXY_IP + "/order/orders", headers=self.headers, verify=False
        )
        for order in resp.json():
            if order["id"] == order_id and order["status"] == order_status[0]["name"]:
                return True

        return False
