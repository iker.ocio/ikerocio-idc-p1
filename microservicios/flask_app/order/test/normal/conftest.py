import base64
import requests
import pytest
from application import create_app, db
from application import models

urls = {"auth": "http://192.168.17.12:13001", "delivery": "http://192.168.17.14:13003"}


@pytest.fixture(scope="session")
def supply_auth_url():
    return urls["auth"]


# esto habria que hacerlo en un session o con un if dependiendo del tipo de test
# @pytest.fixture(scope="session", autouse=True)
# def test_debug():
#    import os
#    test_type = os.getenv("REMOTE_DEBUG")
#    if test_type == "True":
#        import ptvsd
#
#        ptvsd.enable_attach(address=("0.0.0.0", 14001))
#        ptvsd.wait_for_attach()


@pytest.fixture(scope="session")
def create_token(supply_auth_url):
    base64_credentials = base64.b64encode(b"admin:admin").decode("utf-8")
    headers = {"Authorization": "Basic " + base64_credentials}
    url = supply_auth_url + "/auth/authenticate"
    resp = requests.post(url, headers=headers)
    return resp.json()["access_token"]


@pytest.fixture(scope="session")
def seed_database(create_token, test_app):
    import json

    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    client.post(
        "/order",
        headers=headers,
        data=json.dumps(
            {
                "client_id": "admin",
                "number_of_pieces": 4,
                "type": "type_a",
                "address": "01",
            }
        ),
        content_type="application/json",
    )


@pytest.fixture(scope="function")
def reset_database():
    models.Base.metadata.drop_all(db.engine)
    models.Base.metadata.create_all(db.engine)


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    with app.app_context():
        yield app

