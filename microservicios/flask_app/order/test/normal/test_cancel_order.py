import json

"""
docker exec microservicios_order_1 python -m pytest 'test/test_cancel_order.py' -p no:warnings --cov=application -s
"""


def test_cancel_order(create_token, test_app, seed_database):
    order_id = 1
    client_id = "admin"
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.delete(
        "/order/" + str(order_id),
        headers=headers,
        data=json.dumps({"client_id": client_id}),
        content_type="application/json",
    )
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    assert response_json["cancelled"] == "requested"
