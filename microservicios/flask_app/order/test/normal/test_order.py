from application import businessLogic
import json

"""
docker exec microservicios_order_1 python -m pytest 'test/test_order.py' -p no:warnings --cov=application -s
"""

keys = [
    "cancel_request",
    "client_id",
    "creation_date",
    "delivery_id",
    "id",
    "number_of_pieces",
    "pieces_manufactured",
    "status",
    "timestamp",
    "type",
    "update_date",
]


def test_view_orders_with_permission(create_token, test_app):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/order/orders", headers=headers)
    assert resp.status_code == 200


def test_view_order_id(create_token, test_app, seed_database):
    id_to_test = "1"
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/order/" + id_to_test, headers=headers)
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    assert response_json["id"] == int(id_to_test)


def test_create_order(create_token, test_app, reset_database):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.post(
        "/order",
        headers=headers,
        data=json.dumps(
            {
                "client_id": "admin",
                "number_of_pieces": 4,
                "type": "type_a",
                "address": "01",
            }
        ),
        content_type="application/json",
    )
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    assert response_json["id"] == 1
    for key in keys:
        assert key in response_json


def test_view_incorrect_order_id(create_token, test_app, monkeypatch):
    def mock_check(jwt):
        return {"sub": "admin", "perms": ["R_ORDER"]}

    monkeypatch.setattr(businessLogic.BusinessLogic, "static_decrypt_jwt", mock_check)

    id_to_test = "1"
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/order/" + id_to_test, headers=headers)
    response_json = json.loads(resp.data.decode())
    assert resp.status_code == 200
    assert response_json["id"] == int(id_to_test)
