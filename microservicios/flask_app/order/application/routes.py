from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError

from .models import Order
from werkzeug.exceptions import (
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
    Unauthorized,
    ServiceUnavailable,
)

from .saga_order import SagaCreateOrder, SagaCancelOrder
from utils.logger import Logger
from .businessLogic import BusinessLogic
from .businessLogic import get_order_id  # noqa
from utils.config import DevelopmentConfig as Config
from utils.BLConsul import BLConsul

logger = Logger("routes")

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

# Order Routes #######################################################################################################
@app.route("/{}".format(config.SERVICE_NAME), methods=["POST"])
def create_order():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "C_ORDER" in decoded_token["perms"]:

            if request.headers["Content-Type"] != "application/json":
                Logger.print(
                    msg="Unsupported media type in create_order", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)

            content = request.json

            if content["client_id"] != decoded_token["sub"]:
                Logger.print(
                    msg="Impersonation attempt in create_order", level=Logger.ERROR
                )
                abort(Unauthorized.code)

            new_order_dict = BusinessLogic.get_instance().create_order(
                content["client_id"], content["number_of_pieces"], content["type"]
            )
            order = Order(
                id=new_order_dict["id"],
                type=new_order_dict["type"],
                number_of_pieces=new_order_dict["number_of_pieces"],
                client_id=new_order_dict["client_id"],
                status=Order.STATUS_INIT,
                delivery_id=new_order_dict["delivery_id"],
                pieces_manufactured=new_order_dict["pieces_manufactured"],
                creation_date=new_order_dict["creation_date"],
                timestamp=new_order_dict["timestamp"],
                update_date=new_order_dict["update_date"],
            )
            print("SACA CREATED..")
            print(content)
            SagaCreateOrder(order, content["address"])
            print("END")
            response = jsonify(new_order_dict)
            logger.print(msg="POST /order", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in create_order", level=Logger.ERROR
            )
            abort(Unauthorized.code)

    except Exception as e:  # (KeyError, DecodeError, ExpiredSignatureError):
        print(e)
        logger.print(msg="Token error in create_order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/orders".format(config.SERVICE_NAME), methods=["GET"])
@app.route("/orders", methods=["GET"])
def view_orders():
    try:
        auth_header = request.headers["Authorization"]
        # decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        decoded_token = BusinessLogic.static_decrypt_jwt(auth_header)
        if "U_ORDER" in decoded_token["perms"]:
            orders = BusinessLogic.get_instance().get_all_order()
            response = jsonify(Order.list_as_dict(orders))
        else:
            logger.print(
                msg="Not enough permissions in view_orders", level=Logger.ERROR
            )
            abort(Unauthorized.code)
        logger.print(msg="GET /order /orders", level=Logger.DEBUG)
        return response
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_orders", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<int:id>".format(config.SERVICE_NAME), methods=["GET"])
def view_order(id):
    def get_get_get(order_id):
        print("#" * 20)
        print(order_id)
        print(type(order_id))
        return BusinessLogic.get_instance().get_order(id)

    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_ORDER" in decoded_token["perms"]:
            # order = BusinessLogic.get_instance().get_order(id)
            order = get_get_get(id)
            if order is None:
                logger.print(msg="Not found in view_order", level=Logger.ERROR)
                abort(NotFound.code)
            elif order.client_id != decoded_token["sub"]:
                logger.print(
                    msg="Impersonation attempt in view_order", level=Logger.ERROR
                )
                abort(Unauthorized.code)
            print("GET Order {}: {}".format(id, order))
            response = jsonify(order.as_dict())
            logger.print(msg="GET /order/<int:id>", level=Logger.DEBUG)
            return response
        else:
            logger.print(msg="Not enough permissions in view_order", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<int:id>".format(config.SERVICE_NAME), methods=["PUT"])
def update_order(id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_ORDER" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                logger.print(
                    msg="Unsupported media type in update_order", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)
            content = request.json
            status = content["status"]
            order_dict = BusinessLogic.get_instance().update_order(id, status)
            if order_dict is None:
                logger.print(msg="Bad request in update_order", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                logger.print(msg="PUT /order/<int:id>", level=Logger.DEBUG)
                response = jsonify(order_dict)

            logger.print(msg="POST /order/<int:id>", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in update_order", level=Logger.ERROR
            )
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in update_order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<int:id>".format(config.SERVICE_NAME), methods=["DELETE"])
def cancel_order(id):
    print("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA")
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "D_ORDER" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                Logger.print(
                    msg="Unsupported media type in cancel_order", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)

            content = request.json

            if content["client_id"] != decoded_token["sub"]:
                Logger.print(
                    msg="Impersonation attempt in cancel_order", level=Logger.ERROR
                )
                abort(Unauthorized.code)

            SagaCancelOrder(content["client_id"], id)
            status = BusinessLogic.get_instance().get_order(id).status
            if status not in Order.CANCELLABLE_STATUS:
                response = jsonify(dict(cancelled="order not cancellable: " + status))
            else:
                response = jsonify(dict(cancelled="requested"))
            logger.print(msg="DELETE /order", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in cancel_order", level=Logger.ERROR
            )
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError, Exception):
        logger.print(msg="Token error in cancel_order", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/order/health", methods=["HEAD", "GET"])
@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    if not BusinessLogic.get_instance().get_up_status():
        logger.print(msg="Service unavailable", level=Logger.ERROR)
        abort(ServiceUnavailable)

    logger.print(msg="GET /health", level=Logger.HEALTH)
    return "OK"


# Error Handling #####################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    logger.print(
        str({"error_code": e.code, "error_message": e.description, "request": request}),
        level=Logger.ERROR,
    )
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
