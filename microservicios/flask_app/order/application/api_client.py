import requests
import json
from os import environ
from utils.config import DevelopmentConfig as Config
from utils.BLConsul import BLConsul

# Only needed for developing, on production Docker .env file is used
# load_dotenv()

headers = {"Content-type": "application/json"}
config = Config.get_instance()
bl_consul = BLConsul.get_instance()


def request_payment(balance, client_id):
    payment_data = {"balance": balance}
    # payment_response = requests.put(url=environ.get("PAYMENT_IP") + "/payment/" + str(client_id),
    #                                 data=json.dumps(payment_data),
    #                                 headers=headers,
    #                                 verify=False)
    payment_response = requests.put(
        url="https://"
        + environ.get("HAPROXY_IP")
        + ":"
        + environ.get("HAPROXY_PORT")
        + "/payment/"
        + str(client_id),
        data=json.dumps(payment_data),
        headers=headers,
        verify=environ.get("API_CA_LOCATION"),
    )

    return payment_response


def create_piece(order_id, piece_id, status):
    machine_data = {"order_id": order_id, "piece_id": piece_id, "status": status}

    # machine_response = requests.post(url=environ.get("MACHINE_IP") + "/machine/manufacture",
    #                                 data=json.dumps(machine_data),
    #                                 headers=headers,
    #                                 verify=False)
    machine_response = requests.post(
        url="https://"
        + environ.get("HAPROXY_IP")
        + ":"
        + environ.get("HAPROXY_PORT")
        + "/machine/manufacture",
        data=json.dumps(machine_data),
        headers=headers,
        verify=environ.get("API_CA_LOCATION"),
    )
    return machine_response


def create_delivery(order_id, user_id, address):
    delivery_data = {"address": address, "user_id": user_id, "order_id": order_id}

    # delivery_response = requests.post(url=environ.get("DELIVERY_IP") + "/delivery",
    #                                   data=json.dumps(delivery_data),
    #                                   headers=headers,
    #                                   verify=False)
    delivery_response = requests.post(
        url="https://"
        + environ.get("HAPROXY_IP")
        + ":"
        + environ.get("HAPROXY_PORT")
        + "/delivery",
        data=json.dumps(delivery_data),
        headers=headers,
        verify=environ.get("API_CA_LOCATION"),
    )
    return delivery_response


def update_delivery_status(delivery_id, status):
    delivery_data = {"status": status}
    # delivery_response = requests.put(url=environ.get("DELIVERY_IP") + "/delivery/" + str(delivery_id),
    #                                  data=json.dumps(delivery_data),
    #                                  headers=headers,
    #                                  verify=False)

    delivery_response = requests.put(
        url="https://"
        + environ.get("HAPROXY_IP")
        + ":"
        + environ.get("HAPROXY_PORT")
        + "/delivery/"
        + str(delivery_id),
        data=json.dumps(delivery_data),
        headers=headers,
        verify=environ.get("API_CA_LOCATION"),
    )
    return delivery_response


def get_delivery_id(order_id):
    # delivery_response = requests.get(url=environ.get("DELIVERY_IP") + "/delivery/order/" + str(order_id),
    #                                  verify=False)
    delivery_response = requests.get(
        url="https://"
        + environ.get("HAPROXY_IP")
        + ":"
        + environ.get("HAPROXY_PORT")
        + "/delivery/order/"
        + str(order_id),
        verify=environ.get("API_CA_LOCATION"),
    )
    return delivery_response.json()


def get_auth_public_key():
    ret_message, status_code = external_service_response("auth", "publickey")
    if status_code != 200:
        return None
    json_tree = json.loads(ret_message)
    resp = json_tree["response"]
    json_tree = json.loads(resp)
    return json_tree


def external_service_response(external_service_name, path):
    service = bl_consul.get_service(external_service_name)
    service["Name"] = external_service_name
    if service["Address"] is None or service["Port"] is None:
        ret_message = "The service does not exist or there is no healthy replica"
        status_code = 404
    else:
        service["Path"] = path
        ret_message, status_code = call_external_service(service)
    return ret_message, status_code


def call_external_service(service):
    url = "http://{host}:{port}/{service}/{path}".format(
        host=service["Address"],
        port=service["Port"],
        service=service["Name"],
        path=service["Path"],
    )
    response = requests.get(url)
    if response:
        ret_message = json.dumps(
            {
                "caller": config.SERVICE_NAME,
                "callerURL": "{}:{}".format(config.IP, config.PORT),
                "answerer": service["Name"],
                "answererURL": "{}:{}".format(service["Address"], service["Port"]),
                "response": response.text,
                "status_code": response.status_code,
            }
        )
        status_code = response.status_code
    else:
        ret_message = "Could not get message"
        status_code = 500
    return ret_message, status_code
