import json
import os
from utils.logger import Logger
from .models import Order
from utils.EventPublisher import EventPublisher
from .businessLogic import BusinessLogic

logger = Logger("order_performer")
app_settings = os.getenv("APP_STAGE")

eventPublisherDelivery = EventPublisher("delivery_performer", "fanout")


def callback(rawMsg):
    logger.print("Order callback: " + str(rawMsg), level=Logger.INFO)
    jsonMsg = json.loads(rawMsg)
    status = jsonMsg["status"]
    if status == "Created":
        BusinessLogic.get_instance().update_order(
            int(jsonMsg["order_id"]), Order.STATUS_CREATED
        )
        order = BusinessLogic.get_instance().get_order(int(jsonMsg["order_id"]))
        if order is not None:
            eventPublisherDelivery.send_data(
                json.dumps(
                    {
                        "delivery_type": "finish_order",
                        "delivery_id": order.delivery_id,
                        "exchange_response": "order_saga",
                        "order_id": order.id,
                    }
                ),
                "",
            )
        logger.print("Starting delivery 2", Logger.INFO)

    elif status == "Delivered":
        logger.print("event: delivery_finish", level=Logger.INFO)
        BusinessLogic.get_instance().update_order(
            int(jsonMsg["order_id"]), Order.STATUS_DELIVERED
        )
        BusinessLogic.get_instance().update_order(
            int(jsonMsg["order_id"]), Order.STATUS_FINISHED
        )
    elif status == "Queued to store":
        logger.print("event: queued_to_store", level=Logger.INFO)
    elif status == "Being manufactured":
        logger.print("event: being_manufactured", level=Logger.INFO)
    else:
        logger.print("event: not recognized", level=Logger.INFO)


if app_settings != "utils.config.TestingConfig":
    from utils.EventHandler import EventHandler

    EventHandler(
        exchange="order_performer", routing_key="", type="fanout", callbackFunc=callback
    )
