from .saga_states import OrderOrderedState, OrderCancellRequestState
from utils.EventHandler import EventHandler
from .models import Sagas
from .businessLogic import BusinessLogic
import json


class SagaCreateOrder(object):
    def on_event(self, event):
        # The next state will be the result of the on_event function.
        state = self.state.on_event(event)
        if state is not None:
            self.state = state

    def callback(self, body):
        jsonMsg = json.loads(body)
        self.on_event(jsonMsg["event"])

    def __init__(self, order, address):
        """ Initialize the components. """

        # Start with a default state.
        bl = BusinessLogic.get_instance()
        EventHandler(
            exchange="order_saga",
            routing_key="",
            type="fanout",
            callbackFunc=self.callback,
        )
        sagas = Sagas(order_id=order.id, status=Sagas.STATUS_ORDER_ORDERED)
        sagas_dict = bl.create_sagas(order.id, sagas)
        self.state = OrderOrderedState(order, address, sagas_dict["id"])


class SagaCancelOrder(object):
    def on_event(self, event):
        # The next state will be the result of the on_event function.
        state = self.state.on_event(event)
        if state is not None:
            self.state = state

    def callback(self, body):
        jsonMsg = json.loads(body)
        print(str(jsonMsg))
        self.on_event(jsonMsg["event"])

    def __init__(self, client_id, order_id):
        """ Initialize the components. """

        # Start with a default state.
        bl = BusinessLogic.get_instance()
        EventHandler(
            exchange="store_remove_done",
            routing_key="",
            type="fanout",
            callbackFunc=self.callback,
        )
        sagas = Sagas(order_id=order_id, status=Sagas.STATUS_ORDER_CANCEL_REQUEST)
        sagas_dict = bl.create_sagas(order_id, sagas)
        self.state = OrderCancellRequestState(
            client_id, order_id, sagas_dict["id"], self
        )
