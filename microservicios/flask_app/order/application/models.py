from sqlalchemy import Column, DateTime, Integer, String, ForeignKey, Boolean
from sqlalchemy.orm import relationship
from sqlalchemy.sql import func
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class BaseModel(Base):
    __abstract__ = True
    creation_date = Column(DateTime(timezone=True), server_default=func.now())
    update_date = Column(
        DateTime, nullable=False, server_default=func.now(), onupdate=func.now()
    )

    def __repr__(self):
        fields = ""
        for c in self.__table__.columns:
            if fields == "":
                fields = "{}='{}'".format(c.name, getattr(self, c.name))
            else:
                fields = "{}, {}='{}'".format(fields, c.name, getattr(self, c.name))
        return "<{}({})>".format(self.__class__.__name__, fields)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    @staticmethod
    def list_as_dict(items):
        return [i.as_dict() for i in items]


class Order(BaseModel):
    STATUS_INIT = "Init"
    STATUS_PAYED = "Payed"
    STATUS_APPROVED = "Approved"
    STATUS_STORED = "Queued to store"
    STATUS_ACTIVE = "Being manufactured"
    STATUS_CREATED = "Created"
    STATUS_DELIVERED = "Delivered"
    STATUS_FINISHED = "Finished"
    STATUS_REJECTED = "Rejected"
    STATUS_CANCELED_BAD_ZIP_CODE = "Cancelled - Bad ZIP Code"
    STATUS_CANCELED_NO_MONEY = "Cancelled - No Money"
    STATUS_CANCELED_MANUAL = "Cancelled - Manual Cancel"
    STATUS_CANCELED_MANUAL_REQUESTED = STATUS_CANCELED_MANUAL

    CANCELLABLE_STATUS = [STATUS_INIT, STATUS_APPROVED, STATUS_PAYED, STATUS_ACTIVE]

    __tablename__ = "order"
    id = Column(Integer, primary_key=True)
    number_of_pieces = Column(Integer, nullable=False,)
    client_id = Column(String(256), nullable=False,)
    delivery_id = Column(Integer, nullable=True)
    timestamp = Column(DateTime(timezone=True), server_default=None)
    status = Column(String(256), nullable=False, default=STATUS_INIT)
    pieces_manufactured = Column(Integer, default=0)
    cancel_request = Column(Boolean, default=False)
    type = Column(String(256))


class Sagas(BaseModel):
    STATUS_ORDER_ORDERED = "OrderOrderedState"
    STATUS_PENDING_DELIVERY = "OrderPendingDeliveryState"
    STATUS_ORDER_APPROVED = "OrderApprovedState"
    STATUS_ORDER_REFOUND = "OrderRefoundState"
    STATUS_ORDER_REJECTED = "OrderRejectedState"
    STATUS_ORDER_ABORTED = "OrderAbortedState"

    STATUS_ORDER_CANCEL_REQUEST = "OrderCancelRequestState"
    STATUS_ORDER_PREPAYMENT = "OrderPrePaymentState"
    STATUS_ORDER_POSTPAYMENT = "OrderPostPaymentState"
    STATUS_ORDER_REFOUND_CANCEL = "OrderRefoundCancelState"
    STATUS_ORDER_NOT_CANCELABLE = "OrderNotCancellableState"
    STATUS_ORDER_CANCELLED = "OrderCancelledState"

    __tablename__ = "sagas"
    id = Column(Integer, primary_key=True)
    status = Column(String(256))
    order_id = Column(Integer, ForeignKey("order.id"))
    order = relationship("Order", backref="sagas")
