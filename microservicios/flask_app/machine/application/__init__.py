import json
import time
import os
from flask import Flask

from .machine import Machine
from .api_client import get_auth_public_key
from utils.logger import Logger
from .businessLogic import BusinessLogic
from utils.BLConsul import BLConsul

logger = Logger("__init__")


def callback(rawMsg):
    jsonMsg = json.loads(rawMsg)
    new_piece = {"piece_id": int(jsonMsg["piece_id"]), "status": "Manufacturing"}
    my_machine = Machine.get_instance()
    my_machine.add_piece_to_queue(new_piece)
    logger.print(
        msg="event: piece with id " + str(jsonMsg["piece_id"]) + " in queue",
        level=Logger.INFO,
    )


def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app_settings = os.getenv("APP_STAGE")
    app.config.from_object(app_settings)

    with app.app_context():
        from . import routes  # noqa

        while True:
            logger.print(msg="Trying getting Pubkey", level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                logger.print(msg="Error getting Auth Pubkey", level=Logger.DEBUG)
                time.sleep(20)
                continue

            logger.print(msg="Pubkey succesfully getted", level=Logger.DEBUG)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])

        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)

        if app_settings != "utils.config.TestingConfig":
            from utils.EventHandler import EventHandler

            EventHandler(
                exchange="auth_pubkey",
                routing_key="",
                type="fanout",
                callbackFunc=callbackUpdatePubkey,
            )
            EventHandler(
                exchange="machine", routing_key="", type="fanout", callbackFunc=callback
            )

        BusinessLogic.get_instance().set_up_status(True)

        return app
