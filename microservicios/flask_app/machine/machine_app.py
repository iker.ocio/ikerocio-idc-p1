from application import create_app
from os import environ
from dotenv import load_dotenv

load_dotenv()
app = create_app()
app.app_context().push()

if __name__ == "__main__":
    app.run(
        host="0.0.0.0",
        port=13005,
        ssl_context=(environ.get("API_CERT_LOCATION"), environ.get("API_KEY_LOCATION")),
    )
