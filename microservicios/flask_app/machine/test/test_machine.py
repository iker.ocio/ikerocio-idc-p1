import json

"""
docker exec microservicios_machine_1 python -m pytest 'test' -p no:warnings --cov=application -s
"""


def test_create_piece_with_permission(create_token, test_app):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.post(
        "/machine/manufacture", headers=headers, json={"order_id": 1, "piece_id": "1"},
    )
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    assert response_json["order_id"] == 1
    assert response_json["status"] == "Created"
