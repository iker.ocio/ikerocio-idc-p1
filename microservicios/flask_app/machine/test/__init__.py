import requests
import json


class ClientRequest:

    default_header = {"Content-type": "application/json"}

    @staticmethod
    def request(method, url, payload={}, headers=default_header):

        response_msg = requests.request(
            url=url,
            method=method,
            data=json.dumps(payload),
            headers=headers,
            verify=False,
        )

        return response_msg
