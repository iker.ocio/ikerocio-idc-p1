import json

"""
docker exec microservicios_log_1 python -m pytest 'test' -p no:warnings --cov=application -s
"""

keys = ["creation_date", "date", "id", "level", "message", "service", "update_date"]


def test_views_logger_with_permission(create_token, test_app):
    client = test_app.test_client()
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    resp = client.get("/log/view", headers=headers)
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    if len(response_json) > 0:
        for key in keys:
            for item in response_json:
                assert key in item
