import requests
import json
from utils.config import DevelopmentConfig as Config
from utils.BLConsul import BLConsul

# Only needed for developing, on production Docker .env file is used
# load_dotenv()

headers = {"Content-type": "application/json"}
config = Config.get_instance()
bl_consul = BLConsul.get_instance()


def get_auth_public_key():

    ret_message, status_code = external_service_response("auth", "publickey")
    if status_code != 200:
        return None
    json_tree = json.loads(ret_message)
    resp = json_tree["response"]
    json_tree = json.loads(resp)
    return json_tree


def external_service_response(external_service_name, path):
    service = bl_consul.get_service(external_service_name)
    service["Name"] = external_service_name
    if service["Address"] is None or service["Port"] is None:
        ret_message = "The service does not exist or there is no healthy replica"
        status_code = 404
    else:
        service["Path"] = path
        ret_message, status_code = call_external_service(service)
    return ret_message, status_code


def call_external_service(service):
    url = "http://{host}:{port}/{service}/{path}".format(
        host=service["Address"],
        port=service["Port"],
        service=service["Name"],
        path=service["Path"],
    )
    response = requests.get(url)
    if response:
        ret_message = json.dumps(
            {
                "caller": config.SERVICE_NAME,
                "callerURL": "{}:{}".format(config.IP, config.PORT),
                "answerer": service["Name"],
                "answererURL": "{}:{}".format(service["Address"], service["Port"]),
                "response": response.text,
                "status_code": response.status_code,
            }
        )
        status_code = response.status_code
    else:
        ret_message = "Could not get message"
        status_code = 500
    return ret_message, status_code
