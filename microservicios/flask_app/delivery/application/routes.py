from flask import abort
from flask import current_app as app
from flask import jsonify, request
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import (
    BadRequest,
    InternalServerError,
    NotFound,
    ServiceUnavailable,
    Unauthorized,
    UnsupportedMediaType,
)

from utils.BLConsul import BLConsul
from utils.config import DevelopmentConfig as Config
from utils.logger import Logger

from .api_client import update_order
from .businessLogic import BusinessLogic
from .models import Delivery

logger = Logger("routes")

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

# Delivery Routes ####################################################################################################
@app.route("/{}/create".format(config.SERVICE_NAME), methods=["POST"])
def create_delivery():
    try:
        auth_header = request.headers["Authorization"]
        # decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        decoded_token = BusinessLogic.static_decrypt_jwt(auth_header)
        if "C_DELIVERY" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                logger.print(
                    msg="Unsupported media type in create_delivery", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)
            content = request.json

            new_delivery_dict = BusinessLogic.get_instance().create_delivery(
                order_id=content["order_id"],
                user_id=content["user_id"],
                address=content["address"],
            )
            if new_delivery_dict is None:
                logger.print(msg=" Not found in create_delivery", level=Logger.ERROR)
                abort(NotFound.code)
            else:
                logger.print(msg="POST /delivery", level=Logger.DEBUG)
                response = jsonify(new_delivery_dict)
            return response
        else:
            logger.print(
                msg="Not enough permissions in create_delivery", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in create_delivery", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<int:delivery_id>".format(config.SERVICE_NAME), methods=["GET"])
def view_delivery(delivery_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            delivery = BusinessLogic.get_instance().get_delivery(delivery_id)
            if delivery.user_id != decoded_token["sub"]:
                logger.print(
                    msg="Impersonation attempt in view_delivery", level=Logger.ERROR
                )
                abort(Unauthorized.code)
            if delivery is None:
                logger.print(msg="Not found in view_delivery", level=Logger.ERROR)
                abort(NotFound.code)
            logger.print(msg="GET /delivery/<int:delivery_id>", level=Logger.DEBUG)
            response = jsonify(delivery.as_dict())
            return response
        else:
            logger.print(
                msg="Not enough permissions in view_delivery", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_delivery", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<int:delivery_id>".format(config.SERVICE_NAME), methods=["PUT"])
def update_delivery(delivery_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "U_DELIVERY" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                logger.print(
                    msg="Unsupported media type in update_delivery", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)
            content = request.json
            status = content["status"]
            delivery_dict = BusinessLogic.get_instance().update_delivery(
                delivery_id, status
            )
            if delivery_dict is None:
                logger.print(msg=" Not found in update_delivery", level=Logger.ERROR)
                abort(NotFound.code)
            if status == Delivery.STATUS_FINISHED:
                update_order(delivery_dict["order_id"], "Delivered")

            logger.print(msg="PUT /delivery/<int:delivery_id>", level=Logger.DEBUG)
            response = jsonify(delivery_dict)
            return response
        else:
            logger.print(
                msg=" Not enough permissions in update_delivery", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_delivery", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/order/<int:order_id>".format(config.SERVICE_NAME), methods=["GET"])
def view_order_delivery(order_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            delivery = BusinessLogic.get_instance().get_order_delivery(order_id)
            if delivery is None:
                logger.print(msg="Not found in view_order_delivery", level=Logger.ERROR)
                abort(NotFound.code)
            logger.print(msg="PUT /delivery/order/<int:order_id>", level=Logger.DEBUG)
            response = jsonify(delivery.as_dict())
            logger.print(msg="GET /delivery/order/<int:order_id>", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in view_order_delivery", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in view_order_delivery", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/health".format(config.SERVICE_NAME), methods=["HEAD", "GET"])
@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    if not BusinessLogic.get_instance().get_up_status():
        logger.print(msg="Service unavailable", level=Logger.ERROR)
        abort(ServiceUnavailable)

    logger.print(msg="GET /health", level=Logger.HEALTH)
    return "OK"


# Error Handling #####################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    logger.print(
        str({"error_code": e.code, "error_message": e.description, "request": request}),
        level=Logger.ERROR,
    )
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
