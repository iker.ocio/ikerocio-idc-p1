import jwt

from utils.logger import Logger

from . import db
from .models import Delivery

logger = Logger("businessLogic")


class BusinessLogic:
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self

    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    @staticmethod
    def static_decrypt_jwt(auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(
            jwt_token, BusinessLogic.__public_key, algorithms="RS256"
        )
        return decoded_token

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(
            jwt_token, BusinessLogic.__public_key, algorithms="RS256"
        )
        return decoded_token

    def set_auth_public_key(self, pubkey, app=None):
        if app is not None:
            self.app = app
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_up_status(self):
        return BusinessLogic.__up_status

    def create_delivery(self, order_id, user_id, address):
        with self.app.app_context():
            session = db.session
            try:
                new_delivery = Delivery(
                    user_id=user_id, order_id=int(order_id), address=address
                )
                session.add(new_delivery)
                session.commit()
                new_delivery_dict = new_delivery.as_dict()
                session.close()
                return new_delivery_dict
            except KeyError:
                session.rollback()
                session.close()
                return None

    def get_delivery(self, delivery_id):
        session = db.session
        delivery = session.query(Delivery).get(delivery_id)
        if not delivery:
            session.rollback()
            session.close()
            return None
        session.close()
        return delivery

    def get_order_delivery(self, order_id):
        session = db.session
        delivery = (
            session.query(Delivery)
            .filter(Delivery.order_id == int(order_id))
            .one_or_none()
        )
        if not delivery:
            session.rollback()
            session.close()
            return None
        session.close()
        return delivery

    def update_delivery(self, delivery_id, status):
        with self.app.app_context():
            session = db.session
            delivery = session.query(Delivery).get(delivery_id)
            if not delivery:
                session.rollback()
                session.close()
                return None
            delivery.status = status
            session.commit()
            delivery_dict = delivery.as_dict()
            session.close()
            return delivery_dict
