import base64
import requests
import pytest
from application import create_app, db

urls = {"auth": "http://192.168.17.12:13001", "delivery": "http://192.168.17.14:13003"}


@pytest.fixture(scope="session")
def supply_auth_url():
    return urls["auth"]


# esto habria que hacerlo en un session o con un if dependiendo del tipo de test
# @pytest.fixture(scope="session", autouse=True)
# def test_debug():
#    import os
#    test_type = os.getenv("REMOTE_DEBUG")
#    if test_type == "True":
#        import ptvsd
#
#        ptvsd.enable_attach(address=("0.0.0.0", 14001))
#        ptvsd.wait_for_attach()


@pytest.fixture(scope="session")
def create_token(supply_auth_url):
    # return "123"
    base64_credentials = base64.b64encode(b"admin:admin").decode("utf-8")
    headers = {"Authorization": "Basic " + base64_credentials}
    url = supply_auth_url + "/auth/authenticate"
    resp = requests.post(url, headers=headers)
    return resp.json()["access_token"]


@pytest.fixture(scope="session")
def test_app():
    app = create_app()
    with app.app_context():
        print("AAA")
        yield app
        print("BBB")


@pytest.fixture(scope="module")
def test_database():
    db.create_all()
    yield db
    db.session.remove()
    db.drop_all()
