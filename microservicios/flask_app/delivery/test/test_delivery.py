from application import businessLogic
import json

"""
docker exec microservicios_delivery_1 python -m pytest 'test/test_delivery.py' -p no:warnings --cov=application -s
"""


def test_create_delivery_with_permission(create_token, test_app, monkeypatch):
    def mock_check(jwt):
        print("MOCKED")
        return {"sub": user_id, "perms": ["C_DELIVERY"]}

    monkeypatch.setattr(businessLogic.BusinessLogic, "static_decrypt_jwt", mock_check)
    client = test_app.test_client()
    # token = create_token
    token = "123"
    headers = {"Authorization": "Bearer " + token}
    user_id = "admin"
    resp = client.post(
        "/delivery/create",
        headers=headers,
        json={"order_id": 1, "user_id": user_id, "address": "01"},
    )
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    assert response_json["status"] == "Created"
    assert response_json["user_id"] == user_id
