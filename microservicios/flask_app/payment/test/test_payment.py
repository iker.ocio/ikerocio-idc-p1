from application import businessLogic
import uuid

"""
docker exec microservicios_payment_1 python -m pytest 'test/test_payment.py' -p no:warnings --cov=application
"""


def test_create_payment(create_token, test_app, monkeypatch):
    user_id = "admin_" + str(uuid.uuid4())

    def mock_check(jwt):
        return {"sub": user_id, "perms": ["C_PAYMENT"]}

    monkeypatch.setattr(businessLogic.BusinessLogic, "static_decrypt_jwt", mock_check)
    client = test_app.test_client()
    # token = create_token
    token = "123"
    headers = {"Authorization": "Bearer " + token}
    resp = client.post(
        "/payment", headers=headers, json={"user_id": user_id, "balance": "20000"},
    )
    assert resp.status_code == 200
