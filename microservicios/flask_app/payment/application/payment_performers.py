import json
import time
from .businessLogic import BusinessLogic  # noqa
from utils.logger import Logger
from random import randint
from utils.EventPublisher import EventPublisher

logger = Logger("__payment_performers__")


def callbackUpdatePubkey(msgRaw):
    jsonMsg = json.loads(msgRaw)
    BusinessLogic.get_instance().set_auth_public_key(jsonMsg["key"])


def callbackMsg(rawMsg):
    jsonMsg = json.loads(rawMsg)

    time.sleep(randint(9, 10))

    # BYPASS
    status = BusinessLogic.get_instance().update_payment(
        jsonMsg["client_id"], jsonMsg["balance"]
    )

    # status = True # para el BYPASS
    if status:
        event = "payment_done"
        logger.print(
            msg="event: payment done for client id " + str(jsonMsg["client_id"]),
            level=Logger.INFO,
        )
    else:
        logger.print(
            msg="event: payment error for client id " + str(jsonMsg["client_id"]),
            level=Logger.INFO,
        )
        event = "payment_error"
    exchange_response = jsonMsg["exchange_response"]
    eventPublisher = EventPublisher(exchange_response, "fanout")
    eventPublisher.send_data(json.dumps({"event": event}), "")
