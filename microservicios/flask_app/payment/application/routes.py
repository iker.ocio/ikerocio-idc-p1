from flask import request, jsonify, abort
from flask import current_app as app
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import (
    NotFound,
    InternalServerError,
    BadRequest,
    UnsupportedMediaType,
    HTTPException,
    Unauthorized,
    ServiceUnavailable,
)
from .businessLogic import BusinessLogic
from utils.EventPublisher import EventPublisher
from utils.logger import Logger
from utils.config import DevelopmentConfig as Config
from utils.BLConsul import BLConsul

logger = Logger("routes")

config = Config.get_instance()
bl_consul = BLConsul.get_instance()

eventPublisher = EventPublisher("payment", "fanout")

# Payment Routes #####################################################################################################
@app.route("/{}".format(config.SERVICE_NAME), methods=["POST"])
def create_payment():
    try:
        auth_header = request.headers["Authorization"]
        # decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        decoded_token = BusinessLogic.static_decrypt_jwt(auth_header)

        if "C_PAYMENT" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                logger.print(
                    msg="Unsupported media type in create_payment", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)
            content = request.json
            if content["user_id"] != decoded_token["sub"]:
                logger.print(
                    msg="Impersonation attempt in create_payment", level=Logger.ERROR
                )
                abort(Unauthorized.code)
            new_payment_dict = BusinessLogic.get_instance().create_payment(
                content["user_id"], content["balance"]
            )
            if new_payment_dict is None:
                logger.print(msg="Bad request in create_payment", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                eventPublisher.send_data(
                    data="POST new Payment created: " + str(new_payment_dict),
                    routing_key="",
                )
                response = jsonify(new_payment_dict)
            logger.print(msg="POST /payment", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in create_payment", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in create_payment", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<string:user_id>".format(config.SERVICE_NAME), methods=["GET"])
def view_payment(user_id):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_DELIVERY" in decoded_token["perms"]:
            payment = BusinessLogic.get_instance().get_payment(user_id)
            if user_id != decoded_token["sub"]:
                logger.print(
                    msg="Impersonation attempt in view_payment", level=Logger.ERROR
                )
                abort(Unauthorized.code)
            if not payment:
                logger.print(msg="Not found in view_payment", level=Logger.ERROR)
                abort(NotFound.code)
            response = jsonify(payment.as_dict())
            logger.print(msg="GET /payment/<string:user_id>", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in view_payment", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Toke error in view_payment", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/<string:user_id>".format(config.SERVICE_NAME), methods=["PUT"])
def update_balance(user_id):
    try:
        auth_header = request.headers["Authorization"]
        # decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        decoded_token = BusinessLogic.static_decrypt_jwt(auth_header)
        if "U_PAYMENT" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                abort(UnsupportedMediaType.code)
            content = request.json
            if user_id != decoded_token["sub"]:
                logger.print(
                    msg="Impersonation attempt in update_balance", level=Logger.ERROR
                )
                abort(Unauthorized.code)
            payment = BusinessLogic.get_instance().update_payment(
                user_id, content["balance"]
            )
            if payment is None:
                eventPublisher.send_data(
                    data="PUT_ERROR changing Payment status", routing_key=""
                )
                logger.print(msg="Not found in update_balance", level=Logger.ERROR)
                abort(NotFound.code)
            else:
                eventPublisher.send_data(
                    data="PUT Payment status changed to: " + str(payment.as_dict()),
                    routing_key="",
                )

            logger.print(msg="PUT /payment/<string:user_id>", level=Logger.DEBUG)
            response = jsonify(payment.as_dict())
            return response
        else:
            logger.print(
                msg="Not enough permissions in update_balance", level=Logger.ERROR
            )
            abort(Unauthorized.code)
    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Toke error in update_balance", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/health", methods=["HEAD", "GET"])
@app.route("/{}/health".format(config.SERVICE_NAME), methods=["HEAD", "GET"])
def health_check():
    if not BusinessLogic.get_instance().get_up_status():
        logger.print(msg="Service unavailable", level=Logger.ERROR)
        abort(ServiceUnavailable)

    logger.print(msg="GET /health", level=Logger.HEALTH)
    return "OK"


# Error Handling #####################################################################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    logger.print(
        str({"error_code": e.code, "error_message": e.description, "request": request}),
        level=Logger.ERROR,
    )
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code


class PaymentRequired(HTTPException):
    code = 402
    description = "<p>Payment required.</p>"


import os  # noqa

app_settings = os.getenv("APP_STAGE")
if app_settings != "utils.config.TestingConfig":
    from .payment_performers import callbackUpdatePubkey, callbackMsg  # noqa
    from utils.EventHandler import EventHandler  # noqa

    EventHandler(
        exchange="auth_pubkey",
        routing_key="",
        type="fanout",
        callbackFunc=callbackUpdatePubkey,
    )
    EventHandler(
        exchange="payment_saga",
        routing_key="",
        type="fanout",
        callbackFunc=callbackMsg,
    )
