import time
import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

from .api_client import get_auth_public_key
from utils.logger import Logger
from utils.BLConsul import BLConsul

logger = Logger("__init__")

db = SQLAlchemy()


def create_app():
    """Construct the core application."""
    app = Flask(__name__, instance_relative_config=False)
    app_settings = os.getenv("APP_STAGE")
    app.config.from_object(app_settings)
    db.init_app(app)

    with app.app_context():

        from .businessLogic import BusinessLogic  # noqa
        from . import routes  # noqa
        from . import models

        while True:
            message = "Trying getting Pubkey"
            logger.print(msg=message, level=Logger.DEBUG)
            pub_key_response = get_auth_public_key()
            if pub_key_response is None:
                message = "Error getting Auth Pubkey"
                logger.print(msg=message, level=Logger.DEBUG)
                time.sleep(20)
                continue

            message = "Pubkey succesfully getted"
            logger.print(msg=message, level=Logger.DEBUG)
            break

        BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"], app)
        # BusinessLogic.get_instance().set_auth_public_key(pub_key_response["key"])
        bl_consul = BLConsul.get_instance()
        bl_consul.init_and_register(app)
        models.Base.metadata.create_all(db.engine)

        BusinessLogic.get_instance().set_up_status(True)
        return app
