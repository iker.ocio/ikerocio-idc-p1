from application import create_app
from os import environ

app = create_app()
print("$$$" * 20)
app.app_context().push()
print(app.app_context())
print("*" * 20)
if __name__ == "__main__":
    app.run(
        host="0.0.0.0",
        port=13002,
        ssl_context=(environ.get("API_CERT_LOCATION"), environ.get("API_KEY_LOCATION")),
    )
