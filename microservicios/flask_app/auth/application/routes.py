import datetime

import jwt
from flask import abort
from flask import current_app as app
from flask import jsonify, request
from jwt import DecodeError, ExpiredSignatureError
from werkzeug.exceptions import (
    BadRequest,
    InternalServerError,
    NotFound,
    ServiceUnavailable,
    Unauthorized,
    UnsupportedMediaType,
)

from utils.BLConsul import BLConsul
from utils.config import DevelopmentConfig as Config
from utils.logger import Logger

from .businessLogic import BusinessLogic
from .models import User

logger = Logger("routes")


config = Config.get_instance()
bl_consul = BLConsul.get_instance()

# Order Routes ###############################################################


@app.route("/{}/user".format(Config.SERVICE_NAME), methods=["POST"])
def create_user():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "C_AUTH" in decoded_token["perms"]:
            if request.headers["Content-Type"] != "application/json":
                logger.print(
                    msg="Unsupported media type in create_user", level=Logger.ERROR
                )
                abort(UnsupportedMediaType.code)
            content = request.json
            username = content["username"]
            password = content["password"]
            new_user_dict = BusinessLogic.get_instance().create_user(username, password)
            if new_user_dict is None:
                logger.print(msg="Bad request in create_user", level=Logger.ERROR)
                abort(BadRequest.code)
            else:
                logger.print(msg="POST /user", level=Logger.DEBUG)
                response = jsonify(new_user_dict)

            BusinessLogic.get_instance().assign_role_to_user("USER_ROLE", username)

            return response
        else:
            print("DSDD")
            logger.print(
                msg="Not enough permissions in create_user", level=Logger.ERROR
            )
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError, Exception):
        logger.print(msg="POST /order", level=Logger.DEBUG)
        abort(Unauthorized.code)


@app.route("/{}/authenticate".format(config.SERVICE_NAME), methods=["POST"])
def get_auth():
    expiration_time_in_seconds = datetime.timedelta(seconds=60 * 15)
    username = request.authorization["username"]
    password = request.authorization["password"]

    if not BusinessLogic.get_instance().check_user(username, password):
        logger.print(msg="Bad credentials in get_auth", level=Logger.ERROR)
        abort(Unauthorized.code)
    # headers = {"alg": "RSA256", "typ": "jwt"}
    payload = {
        "sub": username,
        "exp": datetime.datetime.utcnow() + expiration_time_in_seconds,
        "perms": BusinessLogic.get_instance().get_user_permissions(username),
    }
    private_key = BusinessLogic.get_instance().get_private_key()
    token = jwt.encode(payload, private_key, algorithm="RS256")
    response = jsonify({"access_token": token.decode()})
    logger.print(msg="GET /auth: " + str(token.decode()), level=Logger.DEBUG)
    return response


@app.route("/{}/users".format(config.SERVICE_NAME), methods=["GET"])
def get_all_client():
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_AUTH" in decoded_token["perms"]:
            users = BusinessLogic.get_instance().get_all_users()
            response = jsonify(User.list_as_dict(users))
            logger.print(msg="GET /users /user", level=Logger.DEBUG)
            return response
        else:
            logger.print(
                msg="Not enough permissions in get_all_client", level=Logger.ERROR
            )
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in get_all_client", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/user/<string:username>".format(config.SERVICE_NAME), methods=["GET"])
def get_user(username):
    try:
        auth_header = request.headers["Authorization"]
        decoded_token = BusinessLogic.get_instance().decrypt_jwt(auth_header)
        if "R_AUTH" in decoded_token["perms"]:
            user = BusinessLogic.get_instance().get_user(username)
            if user is None:
                logger.print(msg="Not found in get_user", level=Logger.ERROR)
                abort(NotFound.code)
            logger.print(msg="GET /user/<string:username>", level=Logger.DEBUG)
            response = jsonify(user.as_dict())
            return response

        else:
            logger.print(msg="Not enough permissions in get_user", level=Logger.ERROR)
            abort(Unauthorized.code)

    except (KeyError, DecodeError, ExpiredSignatureError):
        logger.print(msg="Token error in get_user", level=Logger.ERROR)
        abort(Unauthorized.code)


@app.route("/{}/publickey".format(config.SERVICE_NAME), methods=["GET"])
def get_public_key():
    key = BusinessLogic.get_instance().get_public_key()
    logger.print(msg="GET /auth/publickey key: " + key, level=Logger.DEBUG)
    response = jsonify({"key": key})
    return response


@app.route("/{}/health".format(config.SERVICE_NAME), methods=["HEAD", "GET"])
@app.route("/health", methods=["HEAD", "GET"])
def health_check():
    if not BusinessLogic.get_instance().get_up_status():
        logger.print(msg="Service unavailable", level=Logger.HEALTH)
        abort(ServiceUnavailable)
    logger.print(msg="GET /health", level=Logger.DEBUG)
    return "OK"


# Error Handling #############################################################
@app.errorhandler(UnsupportedMediaType)
def unsupported_media_type_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(BadRequest)
def bad_request_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(NotFound)
def resource_not_found_handler(e):
    return get_jsonified_error(e)


@app.errorhandler(InternalServerError)
def server_error_handler(e):
    return get_jsonified_error(e)


def get_jsonified_error(e):
    # traceback.print_tb(e.__traceback__)
    logger.print(
        str({"error_code": e.code, "error_message": e.description, "request": request}),
        level=Logger.ERROR,
    )
    return jsonify({"error_code": e.code, "error_message": e.description}), e.code
