from sqlalchemy import TEXT, Column, DateTime, ForeignKey, Table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql import func

Base = declarative_base()


class BaseModel(Base):
    __abstract__ = True
    creation_date = Column(DateTime(timezone=True), server_default=func.now())
    update_date = Column(
        DateTime, nullable=False, server_default=func.now(), onupdate=func.now()
    )

    def __repr__(self):
        fields = ""
        for c in self.__table__.columns:
            if fields == "":
                fields = "{}='{}'".format(c.name, getattr(self, c.name))
            else:
                fields = "{}, {}='{}'".format(fields, c.name, getattr(self, c.name))
        return "<{}({})>".format(self.__class__.__name__, fields)

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    @staticmethod
    def list_as_dict(items):
        return [i.as_dict() for i in items]


users_roles = Table(
    "users_roles",
    Base.metadata,
    Column("username", TEXT, ForeignKey("user.username")),
    Column("role_name", TEXT, ForeignKey("role.role_name")),
)

roles_permissions = Table(
    "roles_permissions",
    Base.metadata,
    Column("role_name", TEXT, ForeignKey("role.role_name")),
    Column("permission_name", TEXT, ForeignKey("permission.permission_name")),
)


class User(BaseModel):

    __tablename__ = "user"
    username = Column(TEXT, primary_key=True)
    password = Column(TEXT, nullable=False)
    user_roles_relation = relationship(
        "Role", secondary=users_roles, backref=backref("users_roles", lazy="dynamic")
    )


class Role(BaseModel):
    __tablename__ = "role"
    role_name = Column(TEXT, primary_key=True)
    roles_permissions_relation = relationship(
        "Permission",
        secondary=roles_permissions,
        backref=backref("roles_permissions", lazy="dynamic"),
    )


class Permission(BaseModel):

    __tablename__ = "permission"
    permission_name = Column(TEXT, primary_key=True)
    service_name_ref = Column(TEXT, ForeignKey("service.service_name"))


class Service(BaseModel):

    __tablename__ = "service"
    service_name = Column(TEXT, primary_key=True)
    roles = relationship("Permission", backref="permission_name_backref")
