import copy

import bcrypt
import jwt
from Crypto.PublicKey import RSA

from utils.logger import Logger

from . import db
from .models import Permission, Role, Service, User

logger = Logger("businessLogic")


class BusinessLogic:
    __instance = None
    __public_key = None
    __up_status = False

    def __init__(self):
        if BusinessLogic.__instance is not None:
            raise Exception("This class is a singelton")
        else:
            message = "Service initialized"
            logger.print(message, Logger.INFO)
            BusinessLogic.__instance = self

    @staticmethod
    def get_instance():
        if BusinessLogic.__instance is None:
            BusinessLogic()
        return BusinessLogic.__instance

    def decrypt_jwt(self, auth_header):
        args = auth_header.split("Bearer ")
        jwt_token = args[1]
        decoded_token = jwt.decode(
            jwt_token, BusinessLogic.__public_key, algorithms="RS256"
        )
        return decoded_token

    def set_auth_public_key(self, pubkey):
        BusinessLogic.__public_key = pubkey

    def get_auth_public_key(self):
        return BusinessLogic.__public_key

    def set_up_status(self, status):
        BusinessLogic.__up_status = status
        if status:
            message = "Service up"
            logger.print(message, level=Logger.INFO)

    def get_up_status(self):
        return BusinessLogic.__up_status

    def create_user(self, username, password):
        # session = Session()
        new_user = None
        try:
            new_user = User(
                username=username,
                password=bcrypt.hashpw(password.encode(), bcrypt.gensalt()).decode(),
            )

            db.session.add(new_user)
            db.session.commit()
            new_user_dict = new_user.as_dict()
            db.session.close()
            return new_user_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None

    def get_all_users(self):
        # session = Session()
        users = db.session.query(User).all()
        db.session.close()
        return users

    def get_user(self, username):
        # session = Session()
        user = db.session.query(User).get(username)
        if not user:
            db.session.close()
            return None
        db.session.close()
        return user

    def get_public_key(self):
        secret_code = "Monolitics"
        encoded_key = open("rsa_key.pem", "rb").read()
        key_dec = RSA.import_key(encoded_key, passphrase=secret_code)
        return key_dec.publickey().export_key().decode()

    def user_exists(self, username):
        # session = Session()
        user = db.session.query(User).get(username)
        if user:
            return True
        else:
            return False

    def check_user(self, username, password):
        # session = Session()
        user = db.session.query(User).get(username)
        if not user:
            db.session.close()
            return None
        if not bcrypt.checkpw(password.encode(), user.password.encode()):
            db.session.close()
            return None
        db.session.close()
        return user

    def get_private_key(self):
        secret_code = "Monolitics"
        encoded_key = open("rsa_key.pem", "rb").read()
        key_dec = RSA.import_key(encoded_key, passphrase=secret_code)
        return key_dec.export_key().decode()

    def create_role(self, name):
        # session = Session()
        try:
            role = Role(role_name=name,)
            db.session.add(role)
            db.session.commit()
            role_as_dict = role.as_dict()
            db.session.close()
            return role_as_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def create_permission(self, type, serviceName):
        # session = Session()
        try:
            service = db.session.query(Service).get(serviceName)
            perm = Permission(permission_name=type, permission_name_backref=service)
            db.session.add(perm)
            db.session.commit()
            perm_as_dict = perm.as_dict()
            db.session.close()
            return perm_as_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def create_service(self, name):
        # session = Session()
        try:
            service = Service(service_name=name,)
            db.session.add(service)
            db.session.commit()
            service_as_dict = service.as_dict()
            db.session.close()
            return service_as_dict
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def assign_role_to_user(self, roleName, username):
        # session = Session()
        try:
            role = db.session.query(Role).get(roleName)
            user = db.session.query(User).get(username)
            role.users_roles.append(user)
            db.session.commit()
            db.session.close()
            return True
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def assign_permission_to_role(self, permission_name, role):
        # session = Session()
        try:
            role = db.session.query(Role).get(role)
            permission = db.session.query(Permission).get(permission_name)
            permission.roles_permissions.append(role)
            db.session.commit()
            db.session.close()
            return True
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def get_user_roles(self, username):
        # session = Session()
        try:
            user = db.session.query(User).get(username)
            roles = []
            for role in user.user_roles_relation:
                roles.append(role.role_name)
            db.session.commit()
            db.session.close()
            return roles
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def get_role_permissions(self, role_name):
        # session = Session()
        try:
            role = db.session.query(Role).get(role_name)
            perms = []
            for perm in role.roles_permissions_relation:
                perms.append(perm.permission_name)
            db.session.commit()
            db.session.close()
            return perms
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def get_user_permissions(self, username):
        perms = []
        bl = BusinessLogic.get_instance()
        roles = bl.get_user_roles(username)
        for role in roles:
            perms.append(bl.get_role_permissions(role))

        flat_list = [item for sublist in perms for item in sublist]
        return flat_list

    def get_role(self, role_name):
        # session = Session()
        try:
            role = db.session.query(Role).get(role_name)
            db.session.commit()
            db.session.close()
            return role
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def get_permission(self, permission_name):
        # session = Session()
        try:
            perm = db.session.query(Permission).get(permission_name)
            perm_copy = copy.deepcopy(perm)
            db.session.commit()
            db.session.close()
            return perm_copy
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return

    def get_service(self, service_name):
        # session = Session()
        try:
            service = db.session.query(Service).get(service_name)
            db.session.commit()
            db.session.close()
            return service
        except KeyError:
            db.session.rollback()
            db.session.close()
            return None
        return
