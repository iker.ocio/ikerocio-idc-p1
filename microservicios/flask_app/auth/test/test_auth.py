import uuid
import json
import pytest

"""
 docker exec microservicios_auth_1 python -m pytest 'test' -p no:warnings --cov=application -s
"""


@pytest.mark.parametrize(
    "username, password", [["test1", "test1"], ["test2", "test2"], ["test3", "test3"]],
)
def test_create_user_with_permission(test_app, create_token, username, password):
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    client = test_app.test_client()
    username = username + str(uuid.uuid1())
    resp = client.post(
        "/auth/user",
        headers=headers,
        json={"username": username, "password": password},
    )
    assert resp.status_code == 200
    response_json = json.loads(resp.data.decode())
    assert response_json["username"] == username


def test_create_user_without_permission(test_app):
    client = test_app.test_client()
    resp = client.post("/auth/user", json={"username": "test", "password": "test"})
    assert resp.status_code == 401


def test_get_users(test_app, create_token):
    token = create_token
    headers = {"Authorization": "Bearer " + token}
    client = test_app.test_client()
    resp = client.get("/auth/users", headers=headers)
    assert resp.status_code == 200
