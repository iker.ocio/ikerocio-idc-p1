from application.businessLogic import BusinessLogic


def test_business_logic():
    role = BusinessLogic.get_instance().get_role("unknown")
    assert role is None


def test_business_permission():
    role = BusinessLogic.get_instance().get_permission("unknown")
    assert role is None


def test_business_logic_service():
    role = BusinessLogic.get_instance().get_service("unknown")
    assert role is None


def test_business_logic_get_all_users():
    users = BusinessLogic.get_instance().get_all_users()
    assert len(users) >= 0


def test_business_logic_get_user():
    user = BusinessLogic.get_instance().get_user("a")
    assert user is None


def test_business_logic_check_user():
    user = BusinessLogic.get_instance().check_user("admin", "admin")
    assert user is not None


def test_business_logic_get_private_key():
    pk = BusinessLogic.get_instance().get_private_key()
    assert pk is not None
