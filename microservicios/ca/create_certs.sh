#!/bin/bash

#arr=(auth delivery log_service machine order payment store)
arr=(rabbitmq consul haproxy)
for i in "${arr[@]}"
do
   var="_"
   ssl_var="ssl"
   rbt_var="rbt"
   name="aas_$i"
   ssl="$name$var$ssl_var"
   ssl_key="$name$var$ssl_var.key"
   rbt_key="$name$var$rbt_var.key"
   rbt="$name$var$rbt_var"
   ssl_crt="$ssl.crt"
   rbt_crt="$rbt.crt"
   ssl_csr="$ssl.csr"
   rbt_csr="$rbt.csr"
   echo "$ssl"
   openssl genrsa -out $ssl_key 4096
   openssl genrsa -out $rbt_key 4096
   openssl req -new -key $ssl_key -out $ssl_csr -subj "/C=ES/ST=Alava/L=Vitoria/O=Mondragon Unibertsitatea/CN=$i"
   openssl req -new -key $rbt_key -out $rbt_csr -subj "/C=ES/ST=Alava/L=Vitoria/O=Mondragon Unibertsitatea/CN=$i"
   openssl x509 -req -in $ssl_csr -CA "aas_ca.crt" -CAkey "aas_ca.key" -CAcreateserial -out $ssl_crt -days 4098
   openssl x509 -req -in $rbt_csr -CA "aas_ca.crt" -CAkey "aas_ca.key" -CAcreateserial -out $rbt_crt -days 4098
done
