#!/bin/bash

# STEPS TO CREATE STACK
# aws s3 cp ./cloudformation.yaml s3://mac2020-cicd
# ./createStackAWS.sh prueba https://mac2020-cicd.s3.amazonaws.com/cloudformation.yaml

if [ $# -eq 2 ]; then
    #region=$1
    stackName=$1
    s3Url=$2

    echo '## Creating stack...'
    echo 
    aws cloudformation create-stack --stack-name $stackName \
        --template-url $s3Url

    echo '## Waiting for stack creation...'
    while [[ `aws cloudformation describe-stacks --stack-name ${stackName} --query Stacks[0].StackStatus` != *"COMPLETE"* ]]
    do
        sleep 10
        echo '## Waiting for stack creation...'
    done
    aws cloudformation describe-stacks --stack-name $stackName --query Stacks[0]

else
    echo "Error, el numero de parametros introducido no es correcto"
    echo "Utilizar: ./createAWSStack.sh stackName s3Url"
    echo "s3Url could be https://mac2020-cicd.s3.amazonaws.com/cloudformation.yaml"
fi