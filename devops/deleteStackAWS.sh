#!/bin/bash

if [ $# -eq 1 ]; then
    #region=$1
    stackName=$1
     
    aws cloudformation delete-stack --stack-name $stackName
    echo '## Waiting for stack deletion...'
    while [[ `aws cloudformation describe-stacks --stack-name ${stackName} --query Stacks[0].StackStatus` != *"DELETE"* ]]
    do
        sleep 10
        echo '###...'
    done
else
    echo "Error, el numero de parametros introducido no es correcto"
    echo "Utilizar: ./DELETEAWSStack.sh stackName "
fi
