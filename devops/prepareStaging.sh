# esto ejecutar por ssh en el remoto
docker container rm $(docker container ls -qa) -f
cd /home/ec2-user/ikerocio-idc-p1/microservicios
docker-compose -f docker-compose.deployment.yml up -d rabbitmq haproxy consul
docker-compose -f docker-compose.deployment.yml up -d auth
docker-compose -f docker-compose.deployment.yml up -d delivery log machine payment store order

# esto se ejecuta en staging
